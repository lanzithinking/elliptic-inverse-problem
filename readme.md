# Elliptic Inverse Problem

## This is adapted from [dimension-reduced geom-infMCMC](https://bitbucket.org/lanzithinking/dimension-reduced-geom-infmcmc) in FEniCS-2019.1.0.

* Bayesian inverse problems highly rely on efficient and effective inference methods for uncertainty quantification (UQ). 
Infinite-dimensional MCMC algorithms, directly defined on function spaces, are robust under refinement (through discretization, spectral approximation) of physical models. 
Recent development of this class of algorithms has started to incorporate the geometry of the posterior informed by data so that they are capable of exploring complex probability structures, as frequently arise in UQ for PDE constrained inverse problems. 
However, the required geometric quantities, including the Gauss-Newton Hessian operator or Fisher information metric, are usually expensive to obtain in high dimensions. 
On the other hand, most geometric information of the unknown parameter space in this setting is concentrated in an \emph{intrinsic} finite-dimensional subspace. 
To mitigate the computational intensity and scale up the applications of infinite-dimensional geometric MCMC ($\infty$-GMC), we apply geometry-informed algorithms to the intrinsic subspace to probe its complex structure, and simpler methods like preconditioned Crank-Nicolson (pCN) to its geometry-flat complementary subspace.

* In this work, we take advantage of dimension reduction techniques to accelerate the original $\infty$-GMC algorithms. 
More specifically, partial spectral decomposition (e.g. through randomized linear algebra) of the (prior or posterior) covariance operator is used to identify certain number of principal eigen-directions as a basis for the intrinsic subspace. 
The combination of dimension-independent algorithms, geometric information, and dimension reduction yields more efficient implementation, \emph{(adaptive) dimension-reduced infinite-dimensional geometric MCMC}. 
With a small amount of computational overhead, we can achieve over 70 times speed-up compared to pCN using a simulated elliptic inverse problem and an inverse problem involving turbulent combustion.
A number of error bounds comparing various MCMC proposals are presented to predict the asymptotic behavior of the proposed dimension-reduced algorithms.

### Check the [paper](https://www.sciencedirect.com/science/article/pii/S002199911930289X?via%3Dihub).